# Final-TeVeO

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Raul Molina Lopez
* Titulación: Grado en ingeniería Telematica
* Cuenta en laboratorios: molilobo
* Cuenta URJC: r.molina.2020@alumnos.urjc.es
* Video básico (url):https://youtu.be/4T-dNiac78c 
* Video parte opcional (url): https://youtu.be/U4nsahTFhYk
* Despliegue (url): http://molilobo.pythonanywhere.com
* Contraseñas: admin
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria

Implementación de los recursos obligatorios. Estos son:
* /'
   Pagina principal nos aparece un mensaje de bienvenida asi como aparece las camaras con comentarios de la mas nueva a la mas vieja 
* '/camaras'
    En este recurso tenemos cuatro botones,  al pulsarlos se descargarán la información de
de entre todas las cámaras de la base de datos en el centro, ademas apareceran las distintas camaras en tabla.
* '/{id_camara}':
    Página estática de la camara, aparece la iagen de la camara de cuando se pidio ,nos aparece
la información de la cámara, el número de likes y los comentarios ordenados segun su fecha de publicación,ademas podemos accecder a la camara dinamica asi como el acceso a poner comentarios
si el usuario tiene un nombre dejara delr a like 
* '/{id_camara}-dyn'
    Pagina de la camara dinamica hace lo mismo que la camara estatica pero se actualiza
* '/comentario?id={id_camara}'
     En esta pagina puedes poner un comentario  para la cámara cuyo identificador coincida con el indicado en la query string.
* '/{id_camara}.json'
    Este recurso aporta un resumen con los datos principales de la cámara cuyo identificador se haya pedido. Este resumen
es el identificador, el lugar, la url de la imagen, las coordenadas, el número de comentarios y el número de likes puestos para esa cámara.
* '/configuracion'
    Desde esta pagina tenemos acceso a la personalizacion del perfil donde podrás elegir el nombre  el tamaño y la fuente de la letra Tambien podras eliminar la configuración para que no te reconozca la 
aplicación, o pedir la clave para que copiando la url que te dé puedas transferir la configuración en otro navegador. 

## Lista partes opcionales
* Favicon.ico
* Paginacion de las listas 
* Uso de like,
* Terminar sesión para borrar la configuración
* Parseo dos listados de datos en json de Euskadi y de Galicia.
