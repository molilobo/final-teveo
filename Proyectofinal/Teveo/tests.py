from django.test import TestCase, Client
from django.urls import reverse
from http.cookies import SimpleCookie
from .models import Camara, Perfil


class ViewsTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.camara = Camara.objects.create(
            ident='LIS2-C',
            nombre='CAMARA-CGT MADRID_790',
            url='http://infocar.dgt.es/etraffic/data/camaras/790.jpg',
            localizacion='-3.772143,40.393585'
        )
        self.perfil = Perfil.objects.create(
            nombre='User',
            letra='italic',
            tamanyo='normal',
            cookie='cookie'
        )

    def test_index_view(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'plantillas/index.html')

    def test_camaras_view(self):
        response = self.client.get(reverse('camaras'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'plantillas/camaras.html')

    def test_get_coment_view(self):
        response = self.client.get(reverse('coment'), {'id': self.camara.ident})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'plantillas/coment.html')

    def test_config_view(self):
        response = self.client.get(reverse('config'))
        self.assertEqual(response.status_code, 302)  # Redirección esperada

        self.client.cookies = SimpleCookie({'cookie_teveo': self.perfil.cookie})
        response = self.client.get(reverse('config'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'plantillas/configuracion.html')

    def test_get_ayuda(self):
            response = self.client.get(reverse('ayuda'))
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'plantillas/ayuda.html')

    def test_get_json(self):
        response = self.client.get(reverse('get_json', args=[self.camara.ident]))
        self.assertEqual(response.status_code, 200)
        self.assertIn(response['Content-Type'], ['application/json'])

    from django.test import TestCase
    def test_get_camara_view(self):
        response = self.client.get(reverse('get_camara', args=[self.camara.ident]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'plantillas/camara.html')

    def test_get_camara_dyn(self):
        response = self.client.get(reverse('get_dyn', args=[self.camara.ident]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'plantillas/dynamic.html')

    def test_get_css(self):
        response = self.client.get(reverse('get_css'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(response['Content-Type'], ['text/css'])

    def test_get_image(self):
        response = self.client.get(reverse('imagen'), {'idImg': self.camara.ident})
        self.assertEqual(response.status_code, 200)
        self.assertIn(response['Content-Type'], ['image/jpeg'])




