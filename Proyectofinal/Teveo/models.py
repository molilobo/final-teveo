from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Camara(models.Model):
    ident = models.CharField(max_length=25)
    nombre = models.CharField(max_length=60)
    url = models.URLField()  # cambiar a URLField
    localizacion = models.TextField()
    imagen = models.BinaryField(
        blank=True, null=True)

    def __str__(self):
        return str(self.ident) + ' :' + self.nombre + ', ' + self.url + ', ' + self.localizacion

class Perfil(models.Model):
    nombre = models.CharField(max_length=40, default='Anónimo')
    cookie = models.TextField(default='')
    FUENTE = [('serif', 'serif'),
              ('monospace', 'monospace'),
              ('italic', 'italic'),
              ('cursive', 'cursive'),
              ('fantasy', 'fantasy'),
              ('math', 'math')]
    letra = models.CharField(max_length=25, default='italic', choices=FUENTE)
    TAMANYO = [('x-small', 'Extra pequeño'),
               ('small', 'Pequeño'),
               ('medium', 'Normal'),
               ('large', 'Grande'),
               ('x-large', 'Extra grande')]
    tamanyo = models.CharField(default='medium', max_length=10, choices=TAMANYO)

    def __str__(self):
        return self.nombre + ', cookie: ' + self.cookie + ', tamaño y fuente: ' + self.tamanyo + ', ' + self.letra
class Comentario(models.Model):
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    texto = models.TextField(blank=False, max_length=200)
    imagen = models.BinaryField()
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.camara.ident) + ': ' + self.texto + ', ' + str(self.fecha)





class Like(models.Model):
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    user = models.ForeignKey(Perfil, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.camara) + str(self.user)
