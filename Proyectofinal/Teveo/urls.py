from django.urls import path

from . import views

urlpatterns = [
   path('', views.index, name='index'),
    path('css', views.get_css, name='get_css'),
    path('camaras', views.camaras, name='camaras'),
    path('comentario-dyn', views.get_coment_dyn, name='coment_dyn'),
    path('comentario', views.get_coment, name='coment'),
    path('imagen', views.get_img, name='imagen'),
    path('configuracion', views.config, name='config'),
    path('ayuda', views.ayuda, name='ayuda'),
    path('img_random',views.get_random_image, name='get_image'),
    path('change_img', views.change_img),
    path('change_comm', views.change_comm),
    path('time',views.get_time),
    path('<str:id>.json', views.get_json, name='get_json'),
    path('<str:id>-dyn', views.get_camara_dyn, name='get_dyn'),
    path('<str:id>', views.get_camara, name='get_camara'),
]