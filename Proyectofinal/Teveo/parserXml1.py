import urllib.request
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string


class XmlParser(ContentHandler):
    """Class to handle events fired by the SAX parser

    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__(self):
        self.inCamaras = False
        self.inCamara = False
        self.inContent = False
        self.content = ""
        self.id = ""
        self.url = ""
        self.name = ""
        self.coordenadas = ""
        self.camaras = []

    def startElement(self, name, attrs):
        if name == 'camaras':
            self.inCamaras = True
        elif self.inCamaras:
            if name == 'camara':
                self.inCamara = True
            elif self.inCamara:
                if name == 'id':
                    self.inContent = True
                elif name == 'src':
                    self.inContent = True
                elif name == 'lugar':
                    self.inContent = True
                elif name == 'coordenadas':
                    self.inContent = True

    def endElement(self, name):
        global camaras

        if name == 'camaras':
            self.inCamaras = False
        elif self.inCamaras:
            if name == 'camara':
                self.inCamara = False
                self.camaras.append({'id': 'LIS1-'+self.id,
                                     'url': self.url,
                                     'name': self.name,
                                     'coordenadas': self.coordenadas})
            elif self.inCamara:
                if name == 'id':
                    self.id = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'src':
                    self.url = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'lugar':
                    self.name = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'coordenadas':
                    self.coordenadas = self.content
                    self.content = ""
                    self.inContent = False
    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars


class XML_1:
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = XmlParser()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def camaras(self):
        return self.handler.camaras

if __name__ == '__main__':
    stream = urllib.request.urlopen(
        'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml')
    cameras = XML_1(stream.url).camaras()
    for Camara in cameras:
        print(Camara['id'])